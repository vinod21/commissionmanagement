package com.oasys.commisionmanagement.entity;

import java.util.Calendar;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.oasys.commisionmanagement.common.constants.ChargingType;
import com.oasys.commisionmanagement.common.constants.GSTType;
import com.oasys.commisionmanagement.common.constants.HierarchyType;
import com.oasys.commisionmanagement.common.constants.ServiceCharge;
import com.oasys.commisionmanagement.common.constants.SettlementPeriod;
import com.oasys.commisionmanagement.common.entity.Trackable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "custom_commission_setting")
@Data
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
//@Audited
public class CustomCommissionEntity extends Trackable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8963831449102073138L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name="uuid2", strategy="org.hibernate.id.UUIDGenerator")
	@Column(name = "custom_commission_id", updatable = false, nullable = false,length = 16)
	private UUID customCommissionId;

	@Column(name = "hierarchy_type")
	private HierarchyType  hierarchyType;
	
	@Column(name = "service_id", length = 16)
	private UUID  serviceId;
		
	@Column(name = "service_provider_id", length = 16)
	private UUID  serviceProviderId;
	
	@Column(name = "slap_id", length = 16)
	private UUID  slabId;
	
	@Column(name = "agent_id")
	private String  agentId;
			
	// this commission value in flat only
	@Column(name = "admin_commission_value")
	private Double adminCommisionValue;
	
	@Column(name = "dd_commission_value")
	private Double ddCommisionValue;
	
	@Column(name = "pcd_commission_value")
	private Double pcdCommisionValue;
	
	@Column(name = "r_commission_value")
	private Double rCommisionValue;
	
	@Column(name = "charging_type")
	private ChargingType chargingType;
	
	@Column(name = "service_charge")
	private ServiceCharge serviceCharge;
	
	@Column(name = "gst_type")
	private GSTType gstType;
	
	@Column(name = "tds_applicable")
	private boolean tdsApplicable;
	
	@Column(name = "settlement_period")
	private SettlementPeriod settlementPeriod;
	
	@Column(name = "from_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Kolkata")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar  fromDate;
	
	@Column(name = "to_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Kolkata")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar  toDate;
	
	
	@Transient
	private String serviceName;
	@Transient
	private String serviceProviderName;
	@Transient
	private String serviceCategoryName;
	
	@Transient
	private String slot;
	
	
	
}
