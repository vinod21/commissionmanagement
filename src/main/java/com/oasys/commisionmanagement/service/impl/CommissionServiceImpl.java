package com.oasys.commisionmanagement.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.oasys.commisionmanagement.common.cexception.InvalidRequestException;
import com.oasys.commisionmanagement.common.cexception.NoRecordFoundException;
import com.oasys.commisionmanagement.common.constants.HierarchyType;
import com.oasys.commisionmanagement.common.dto.CBaseDTO;
import com.oasys.commisionmanagement.common.dto.CalculateCommissionsDTO;
import com.oasys.commisionmanagement.common.dto.CommissionFilterDto;
import com.oasys.commisionmanagement.common.dto.CustomCommissionDto;
import com.oasys.commisionmanagement.common.dto.CustomCommissionUpdateDto;
import com.oasys.commisionmanagement.common.dto.GlobalCommissionSettingDto;
import com.oasys.commisionmanagement.common.dto.GlobalCommissionUpdateDto;
import com.oasys.commisionmanagement.common.dto.ServiceDeatilsDto;
import com.oasys.commisionmanagement.common.utils.CommonUtil;
import com.oasys.commisionmanagement.entity.CustomCommissionEntity;
import com.oasys.commisionmanagement.entity.DeletedCustomCommissionHistoryEntity;
import com.oasys.commisionmanagement.entity.GlobalCommissionSettingEntity;
import com.oasys.commisionmanagement.feign.dto.CommonBaseDTO;
import com.oasys.commisionmanagement.feign.dto.ServiceAndProviderDto;
import com.oasys.commisionmanagement.repository.CustomCommissionRepository;
import com.oasys.commisionmanagement.repository.DeleteCustomCommissionRepository;
import com.oasys.commisionmanagement.repository.GlobalCommissionSettingRepository;
import com.oasys.commisionmanagement.repository.impl.CommonCommissionRepositoryImpl;
import com.oasys.commisionmanagement.rest.feign.MasterManagementFegin;
import com.oasys.commisionmanagement.service.CommissionService;

@Service
public class CommissionServiceImpl implements CommissionService {

	@Autowired
	GlobalCommissionSettingRepository globalCommissionSettingRepository;

	@Autowired
	CustomCommissionRepository customCommissionRepository;

	@Autowired
	CommonCommissionRepositoryImpl commonCommissionRepositoryImpl;
	
	@Autowired
	DeleteCustomCommissionRepository deleteCustomCommissionRepository;

	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	MasterManagementFegin masterManagementFegin;

	private void validateRequest(GlobalCommissionSettingEntity globalCommisionDto, boolean slapRangeValidation) {
		Double sum = 0.0;
		
		sum = sum + globalCommisionDto.getDdCommisionValue() + globalCommisionDto.getPcdCommisionValue()
					+ globalCommisionDto.getRCommisionValue();
		
		if (sum > globalCommisionDto.getAdminCommisionValue()) {
			throw new InvalidRequestException(
					"The Sum of value of District distributor,Pin code distributor ,Retailer must be less then or equals Admin commission value");
		}
		if (slapRangeValidation) {
			Optional<GlobalCommissionSettingEntity> optional = globalCommissionSettingRepository.findActiveCommission(
					globalCommisionDto.getHierarchyType(), globalCommisionDto.getServiceId(),
					globalCommisionDto.getServiceProviderId(),globalCommisionDto.getSlabId());
			if (optional.isPresent()) {
				throw new InvalidRequestException(String.format("Rquested slap id:{} already present in database for service id:{},serviceProviderId:{},subcategoryId:{}",globalCommisionDto.getSlabId(),
						globalCommisionDto.getServiceId(),globalCommisionDto.getServiceProviderId()));
			}
		}
	}

	private void validateRequestForCustom(CustomCommissionEntity cutCommissionEntity, boolean slapRangeValidation) {
		
		
		Double sum = 0.0;
		
		sum = sum + cutCommissionEntity.getDdCommisionValue() + cutCommissionEntity.getPcdCommisionValue()
					+ cutCommissionEntity.getRCommisionValue();

		if (sum > cutCommissionEntity.getAdminCommisionValue()) {
			throw new InvalidRequestException(
					"The Sum of value of District distributor,Pin code distributor ,Retailer must be less then or equals Admin commission value");
		}
		if (slapRangeValidation) {
			Optional<CustomCommissionEntity> optional = customCommissionRepository.findActiveCommission(
					cutCommissionEntity.getHierarchyType(), cutCommissionEntity.getServiceId(),
					cutCommissionEntity.getServiceProviderId(),cutCommissionEntity.getSlabId(), cutCommissionEntity.getAgentId());
			if (optional.isPresent()) {
				throw new InvalidRequestException(String.format("Rquested slap id:{} already present in database for service id:{},serviceProviderId:{},subcategoryId:{}",cutCommissionEntity.getSlabId(),
						cutCommissionEntity.getServiceId(),cutCommissionEntity.getServiceProviderId()));
			}
		}
	}

	@Override
	public CBaseDTO addGlobalCommission(GlobalCommissionSettingDto globalCommisionDto) {

		GlobalCommissionSettingEntity globalCommissionSettingEntity = commonUtil.modalMap(globalCommisionDto,
				GlobalCommissionSettingEntity.class);
		validateRequest(globalCommissionSettingEntity, true);
		globalCommissionSettingRepository.save(globalCommissionSettingEntity);
		return new CBaseDTO();
	}

	@Override
	public CBaseDTO updateGlobalCommission(GlobalCommissionUpdateDto globalCommisionUpdateDto) {

		Optional<GlobalCommissionSettingEntity> optional = globalCommissionSettingRepository
				.findById(globalCommisionUpdateDto.getGlobalCommissionId());
		if (!optional.isPresent()) {
			throw new NoRecordFoundException(
					"No Record Found for Gloabl Commission Id:" + globalCommisionUpdateDto.getGlobalCommissionId());
		}
		GlobalCommissionSettingEntity globalCommissionSettingEntity = optional.get();
		validateRequest(globalCommissionSettingEntity, false);
		commonUtil.modalMapCopy(globalCommisionUpdateDto, globalCommissionSettingEntity);
		globalCommissionSettingRepository.save(globalCommissionSettingEntity);
		return new CBaseDTO();
	}

	@Override
	public CBaseDTO getGlobalCommissions(CommissionFilterDto globalCommissionFilterDto) {
		CBaseDTO cBaseDTO = new CBaseDTO();
		List<GlobalCommissionSettingEntity> globalCommissionSettingEntities=commonCommissionRepositoryImpl.serachGlobalCommissionSetting(globalCommissionFilterDto);
		List<ServiceDeatilsDto> serviceDeatilsDtos=globalCommissionSettingEntities.
				stream().map(obj->{
					ServiceDeatilsDto serviceDeatilsDto=new ServiceDeatilsDto();
					serviceDeatilsDto.setServiceId(obj.getServiceId());
					serviceDeatilsDto.setServiceProviderId(obj.getServiceProviderId());
					serviceDeatilsDto.setSlabId(obj.getSlabId());
					return serviceDeatilsDto;
				}).collect(Collectors.toList());
		ServiceAndProviderDto serviceAndProviderDto=masterManagementFegin.getServiceAndProviderList(serviceDeatilsDtos);
		
		
		globalCommissionSettingEntities.forEach(obj->
		{
			obj.setServiceName(serviceAndProviderDto.getResponseContent().get("service").
					get(obj.getServiceId().toString()).get(0).get("serviceName").asText());
			
			obj.setServiceProviderName(serviceAndProviderDto.getResponseContent().get("serviceProvider").
					get(obj.getServiceProviderId().toString()).get(0).get("serviceProviderName").asText());
			
			obj.setServiceCategoryName(serviceAndProviderDto.getResponseContent().get("serviceProvider").
					get(obj.getServiceProviderId().toString()).get(0).get("serviceCategoryId").get("name").asText());
		
			JsonNode node=serviceAndProviderDto.getResponseContent().get("slabs").get(obj.getSlabId().toString());
			if(node!=null)
			{
				obj.setSlot(node.get(0).get("slot").asText());
			}	
		});
		cBaseDTO.setResponseContent(globalCommissionSettingEntities);
		return cBaseDTO;
	}

	@Override
	public CBaseDTO addCustomCommission(CustomCommissionDto customCommissionDto) {

		CustomCommissionEntity customCommissionEntity = commonUtil.modalMap(customCommissionDto,
				CustomCommissionEntity.class);

		validateRequestForCustom(customCommissionEntity, true);
		customCommissionRepository.save(customCommissionEntity);
		return new CBaseDTO();
	}

	@Override
	public CBaseDTO updateCustomCommission(CustomCommissionUpdateDto customCommissionUpdateDto) {
		Optional<CustomCommissionEntity> optional = customCommissionRepository
				.findById(customCommissionUpdateDto.getCustomCommissionId());
		if (!optional.isPresent()) {
			throw new NoRecordFoundException(
					"No Record Found for Commission Id:" + customCommissionUpdateDto.getCustomCommissionId());
		}
		CustomCommissionEntity customCommissionEntity = optional.get();
		validateRequestForCustom(customCommissionEntity, false);
		commonUtil.modalMapCopy(customCommissionUpdateDto, customCommissionEntity);
		customCommissionRepository.save(customCommissionEntity);
		return new CBaseDTO();
	}

	@Override
	public CBaseDTO getCustomCommissions(CommissionFilterDto commissionFilterDto) {
		CBaseDTO cBaseDTO = new CBaseDTO();
		List<CustomCommissionEntity> customCommissionEntities=commonCommissionRepositoryImpl.serachCustomCommission(commissionFilterDto);
		
		List<ServiceDeatilsDto> serviceDeatilsDtos=customCommissionEntities.
				stream().map(obj->{
					ServiceDeatilsDto serviceDeatilsDto=new ServiceDeatilsDto();
					serviceDeatilsDto.setServiceId(obj.getServiceId());
					serviceDeatilsDto.setServiceProviderId(obj.getServiceProviderId());
					serviceDeatilsDto.setSlabId(obj.getSlabId());
					return serviceDeatilsDto;
				}).collect(Collectors.toList());
		ServiceAndProviderDto serviceAndProviderDto=masterManagementFegin.getServiceAndProviderList(serviceDeatilsDtos);
		
		
		customCommissionEntities.forEach(obj->
		{
			obj.setServiceName(serviceAndProviderDto.getResponseContent().get("service").
					get(obj.getServiceId().toString()).get(0).get("serviceName").asText());
			
			obj.setServiceProviderName(serviceAndProviderDto.getResponseContent().get("serviceProvider").
					get(obj.getServiceProviderId().toString()).get(0).get("serviceProviderName").asText());
			
			obj.setServiceCategoryName(serviceAndProviderDto.getResponseContent().get("serviceProvider").
					get(obj.getServiceProviderId().toString()).get(0).get("serviceCategoryId").get("name").asText());
		
			JsonNode node=serviceAndProviderDto.getResponseContent().get("slabs").get(obj.getSlabId().toString());
			if(node!=null)
			{
				obj.setSlot(node.get(0).get("slot").asText());
			}	
		});
		cBaseDTO.setResponseContent(customCommissionEntities);
		return cBaseDTO;
		
		
	}

	@Override
	public CBaseDTO getCalculateCommissions(HierarchyType hierarchyType, String agentId, UUID serviceId,
			UUID serviceProvierId,UUID subcategoryId,Double amount) {
		CBaseDTO cBaseDTO = new CBaseDTO();
		CalculateCommissionsDTO calculateCommissionsDTO = new CalculateCommissionsDTO();
		cBaseDTO.setResponseContent(calculateCommissionsDTO);
		CommonBaseDTO commonBaseDTO=masterManagementFegin.getSlot(subcategoryId, amount);
		Optional<CustomCommissionEntity> customOptional = customCommissionRepository
				.findActiveCommissionForAgent(hierarchyType,serviceId,serviceProvierId, commonBaseDTO.getResponseContent().getId(), agentId, commonUtil.getCurrentDate());
		if (customOptional.isPresent()) {
			calculateCommissionsDTO.setType("CUSTOM");
			calculateCommissionsDTO.setCustomCommission(customOptional.get());
			return cBaseDTO;
		}

		Optional<GlobalCommissionSettingEntity> globalOptional = globalCommissionSettingRepository
				.findActiveCommission(hierarchyType, serviceId,serviceProvierId, commonBaseDTO.getResponseContent().getId());
		if (globalOptional.isPresent()) {
			calculateCommissionsDTO.setType("GLOBAL");
			calculateCommissionsDTO.setGlobalCommission(globalOptional.get());
			return cBaseDTO;
		}
		throw new NoRecordFoundException(
				String.format("No active commission found for hierarchyType:{},agentId:{},serviceId:{},amount:{}",
						hierarchyType, agentId, serviceId, amount));
	}

	@Override
	@Transactional
	public CBaseDTO deleteCustomCommission(UUID id) {
		CBaseDTO cBaseDTO=new CBaseDTO();
		Optional<CustomCommissionEntity> optionalData=customCommissionRepository.findById(id);
		if(!optionalData.isPresent())
		{
			return cBaseDTO;
		}
		DeletedCustomCommissionHistoryEntity deletedCustomCommissionHistoryEntity=commonUtil.modalMap(optionalData.get(), DeletedCustomCommissionHistoryEntity.class);
		deletedCustomCommissionHistoryEntity.setModifiedDate(new Date());
		deleteCustomCommissionRepository.save(deletedCustomCommissionHistoryEntity);
		customCommissionRepository.delete(optionalData.get());
		return cBaseDTO;
	}

}
