package com.oasys.commisionmanagement.service.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.security.oauth2.provider.token.UserAuthenticationConverter;
import org.springframework.stereotype.Service;

@Service
@Qualifier("customExtractAuthentication")
public class CustomExtractAuthentication extends DefaultAccessTokenConverter  {

	private UserAuthenticationConverter userTokenConverter = new DefaultUserAuthenticationConverter();
	final String AUTHORITIES = "authorities";
	public final String SCOPE="scope";
	private boolean includeGrantType;
	/*
	 * @Override public OAuth2AccessToken extractAccessToken(String value,
	 * Map<String, ?> map) { Map<String, List<String>> clientAuthorities =
	 * (Map<String, List<String>>) map.get(AUTHOTITIES); Set<String>
	 * scope=(Set<String>)map.get(SCOPE); Map<String,String>
	 * requestParameters=(Map<String,String>)map.get("requestParameters"); String
	 * clientId=(String)map.get("clientId");
	 * 
	 * Collection<GrantedAuthority> grantedAuthorities = new
	 * ArrayList<GrantedAuthority>();
	 * 
	 * for (List<String> grantedAuthority : clientAuthorities.values()) { for
	 * (String authority : grantedAuthority) { grantedAuthorities.add(new
	 * SimpleGrantedAuthority(authority)); } }
	 * 
	 * Set<String> resourceIds = new LinkedHashSet<String>(map.containsKey(AUD) ?
	 * (Collection<String>) map.get(AUD) : Collections.<String> emptySet());
	 * //OAuth2Request request = new OAuth2Request(requestParameters, clientId,
	 * grantedAuthorities, true, scope, resourceIds, null, null, null);
	 * 
	 * OAuth2Request request=new OAuth2Request(requestParameters, clientId,
	 * grantedAuthorities, true, scope, resourceIds, null, null, null);
	 * 
	 * super.extractAccessToken(value, map); }
	 */
	public OAuth2AccessToken extractAccessToken(String value, Map<String, ?> map) {
	 	OAuth2AccessToken accessToken=super.extractAccessToken(value, map);
	 	return accessToken;
	}
	@Override
	public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
		Map<String, String> parameters = new HashMap<String, String>();
		Set<String> scope = extractScope(map);
		Authentication user = userTokenConverter.extractAuthentication(map);
		String clientId = (String) map.get(CLIENT_ID);
		parameters.put(CLIENT_ID, clientId);
		if (includeGrantType && map.containsKey(GRANT_TYPE)) {
			parameters.put(GRANT_TYPE, (String) map.get(GRANT_TYPE));
		}
		Set<String> resourceIds = new LinkedHashSet<String>(map.containsKey(AUD) ? getAudience(map)
				: Collections.<String>emptySet());
		
		Collection<? extends GrantedAuthority> authorities = null;
		if (user==null && map.containsKey(AUTHORITIES)) {
			@SuppressWarnings("unchecked")
			String[] roles = ((Collection<String>)map.get(AUTHORITIES)).toArray(new String[0]);
			authorities = AuthorityUtils.createAuthorityList(roles);
		}
		//append AUTHORITIES to scope 
		if(map.containsKey(AUTHORITIES))
		{
			@SuppressWarnings("unchecked")
			Collection<String> roles=(Collection<String>)map.get(AUTHORITIES);
			scope.addAll(roles);
		}
		OAuth2Request request = new OAuth2Request(parameters, clientId, authorities, true, scope, resourceIds, null, null,
				null);
		return new OAuth2Authentication(request, user);		
	}
	
	private Collection<String> getAudience(Map<String, ?> map) {
		Object auds = map.get(AUD);
		if (auds instanceof Collection) {			
			@SuppressWarnings("unchecked")
			Collection<String> result = (Collection<String>) auds;
			return result;
		}
		return Collections.singleton((String)auds);
	}
	private Set<String> extractScope(Map<String, ?> map) {
		Set<String> scope = Collections.emptySet();
		if (map.containsKey(SCOPE)) {
			Object scopeObj = map.get(SCOPE);
			if (String.class.isInstance(scopeObj)) {
				scope = new LinkedHashSet<String>(Arrays.asList(String.class.cast(scopeObj).split(" ")));
			} else if (Collection.class.isAssignableFrom(scopeObj.getClass())) {
				@SuppressWarnings("unchecked")
				Collection<String> scopeColl = (Collection<String>) scopeObj;
				scope = new LinkedHashSet<String>(scopeColl);	// Preserve ordering
			}
		}
		return scope;
	}
	
	
	

	

	
}
