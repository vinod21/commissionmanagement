package com.oasys.commisionmanagement.service;

import java.util.UUID;

import com.oasys.commisionmanagement.common.constants.HierarchyType;
import com.oasys.commisionmanagement.common.dto.CBaseDTO;
import com.oasys.commisionmanagement.common.dto.CommissionFilterDto;
import com.oasys.commisionmanagement.common.dto.CustomCommissionDto;
import com.oasys.commisionmanagement.common.dto.CustomCommissionUpdateDto;
import com.oasys.commisionmanagement.common.dto.GlobalCommissionSettingDto;
import com.oasys.commisionmanagement.common.dto.GlobalCommissionUpdateDto;

public interface CommissionService {

	public CBaseDTO addGlobalCommission(GlobalCommissionSettingDto globalCommisionDto);

	public CBaseDTO updateGlobalCommission(GlobalCommissionUpdateDto globalCommisionUpdateDto);

	public CBaseDTO getGlobalCommissions(CommissionFilterDto globalCommissionFilterDto);
	
	public CBaseDTO addCustomCommission(CustomCommissionDto customCommissionDto);
	public CBaseDTO updateCustomCommission(CustomCommissionUpdateDto customCommissionUpdateDto);
	
	public CBaseDTO getCustomCommissions(CommissionFilterDto commissionFilterDto);
	
	public CBaseDTO getCalculateCommissions(HierarchyType hierarchyType, String agentId, UUID serviceId,
			UUID serviceProvier0Id,UUID subcategoryId,Double amount);
	
	public CBaseDTO deleteCustomCommission(UUID id);

}
