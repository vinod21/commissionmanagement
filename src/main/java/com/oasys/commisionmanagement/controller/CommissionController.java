	package com.oasys.commisionmanagement.controller;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oasys.commisionmanagement.common.constants.HierarchyType;
import com.oasys.commisionmanagement.common.dto.CBaseDTO;
import com.oasys.commisionmanagement.common.dto.CommissionFilterDto;
import com.oasys.commisionmanagement.common.dto.CustomCommissionDto;
import com.oasys.commisionmanagement.common.dto.CustomCommissionUpdateDto;
import com.oasys.commisionmanagement.common.dto.GlobalCommissionSettingDto;
import com.oasys.commisionmanagement.common.dto.GlobalCommissionUpdateDto;
import com.oasys.commisionmanagement.service.CommissionService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/commission")
public class CommissionController {

	@Autowired
	CommissionService commissionService;

	@ApiOperation(value = "this API is use add new global commission", response = CBaseDTO.class)
	@PostMapping("/global")
	@PreAuthorize("#oauth2.hasScope('Admin')")
	public CBaseDTO addCommissionPlan(@RequestBody @Valid GlobalCommissionSettingDto globalCommissionSettingDto) {
		return commissionService.addGlobalCommission(globalCommissionSettingDto);
	}

	@ApiOperation(value = "this API is use to update global commission", response = CBaseDTO.class)
	@PutMapping("/global")
	@PreAuthorize("#oauth2.hasScope('Admin')")
	public CBaseDTO updateCommissionPlan(@RequestBody @Valid GlobalCommissionUpdateDto globalCommissionUpdateDto) {
		return commissionService.updateGlobalCommission(globalCommissionUpdateDto);
	}

	@ApiOperation(value = "this API is use to get all global commission list", response = CBaseDTO.class)
	@PostMapping("/global/get")
	@PreAuthorize("#oauth2.hasScope('Admin')")
	public CBaseDTO getCommission(@RequestBody @Valid CommissionFilterDto globalCommissionFilterDto) {
		return commissionService.getGlobalCommissions(globalCommissionFilterDto);
	}
	
	
	@ApiOperation(value = "this API is use add new custom commission", response = CBaseDTO.class)
	@PostMapping("/custom")
	@PreAuthorize("#oauth2.hasScope('Admin')")
	public CBaseDTO addCustomCommissionPlan(@RequestBody @Valid CustomCommissionDto customCommissionDto) {
		return commissionService.addCustomCommission(customCommissionDto);
	}

	@ApiOperation(value = "this API is use update custom commission", response = CBaseDTO.class)
	@PutMapping("/custom")
	@PreAuthorize("#oauth2.hasScope('Admin')")
	public CBaseDTO updateCustomCommissionPlan(@RequestBody @Valid CustomCommissionUpdateDto customCommissionUpdateDto) {
		return commissionService.updateCustomCommission(customCommissionUpdateDto);
	}

	@ApiOperation(value = "this API is use to get all custom commission", response = CBaseDTO.class)
	@PostMapping("/custom/get")
	@PreAuthorize("#oauth2.hasScope('Admin')")
	public CBaseDTO getCustomCommission(@RequestBody @Valid CommissionFilterDto globalCommissionFilterDto) {
		return commissionService.getCustomCommissions(globalCommissionFilterDto);
	}
	
	@ApiOperation(value = "this API is use to delete custom commission", response = CBaseDTO.class)
	@DeleteMapping("/custom/{id}")
	@PreAuthorize("#oauth2.hasScope('Admin')")
	public CBaseDTO deleteCustomCommission(@PathVariable("id") UUID id) {
		return commissionService.deleteCustomCommission(id);
	}
	
	@ApiOperation(value = "this API is get commission details based on transaction amount", response = CBaseDTO.class)
	@PostMapping("/get/{hierarchyType}/{agentId}/{serviceId}/{serviceProvierId}/{subcategoryId}/{amount}")
	@PreAuthorize("isAuthenticated()")
	public CBaseDTO getCommission(@PathVariable("hierarchyType") HierarchyType hierarchyType,
			@PathVariable("agentId") String agentId,@PathVariable("serviceId") UUID serviceId,
			@PathVariable("serviceProvierId") UUID serviceProvierId,@PathVariable("subcategoryId") UUID subcategoryId,@PathVariable("amount") Double amount) {
		return commissionService.getCalculateCommissions(hierarchyType, agentId, serviceId, serviceProvierId, subcategoryId, amount);
	}
	
	

}
