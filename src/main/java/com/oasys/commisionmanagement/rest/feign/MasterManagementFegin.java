package com.oasys.commisionmanagement.rest.feign;

import java.util.List;
import java.util.UUID;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.oasys.commisionmanagement.common.dto.ServiceDeatilsDto;
import com.oasys.commisionmanagement.feign.config.InternalFeginAPIConfig;
import com.oasys.commisionmanagement.feign.dto.CommonBaseDTO;
import com.oasys.commisionmanagement.feign.dto.ServiceAndProviderDto;



@FeignClient(name = "MasterManagementFeign", url = "${master.management.host}",configuration =InternalFeginAPIConfig.class)
public interface MasterManagementFegin {

	String AUTH_TOKEN = "X-Authorization";
	
	@GetMapping("/merchanttype/get/{id}")
	public CommonBaseDTO getAgentById(@RequestHeader(AUTH_TOKEN) String bearerToken, @PathVariable UUID id);

	@GetMapping("/role/get/{id}")
	public CommonBaseDTO getRoleById(@RequestHeader(AUTH_TOKEN) String bearerToken, @PathVariable UUID id);

	@GetMapping("/role/getbyname/{name}")
	public CommonBaseDTO getRoleByName(@RequestHeader(AUTH_TOKEN) String bearerToken, @PathVariable String name);

	@GetMapping("servicecategory/getallactive")
	public CommonBaseDTO getServiceCategory(@RequestHeader(AUTH_TOKEN) String bearerToken);
	
	@GetMapping("/servicecategory/get/{id}")
	public CommonBaseDTO getCategoryById(@RequestHeader(AUTH_TOKEN) String bearerToken, @PathVariable UUID id);
	
	@GetMapping("/serviceprovider/get/{id}")
	public CommonBaseDTO getServiceProviderById(@RequestHeader(AUTH_TOKEN) String bearerToken, @PathVariable UUID id);
	
	@GetMapping("/subCategory/get/{id}")
	public CommonBaseDTO getSubCategoryById(@RequestHeader(AUTH_TOKEN) String bearerToken, @PathVariable UUID id);
	
	
	@GetMapping("/serviceSlot/get/{id}")
	public CommonBaseDTO getSloById(@RequestHeader(AUTH_TOKEN) String bearerToken, @PathVariable UUID id);
	
	@GetMapping("/serviceSlot/get/{serviceCategoryId}/{amount}")
	public CommonBaseDTO getSlot(@PathVariable("serviceCategoryId") UUID id,@PathVariable("amount") Double amount);
	
	@PostMapping("/service/getServiceAndProviderList")
	public ServiceAndProviderDto getServiceAndProviderList(@RequestBody List<ServiceDeatilsDto> serviceDeatilsDto);
	
	
}
