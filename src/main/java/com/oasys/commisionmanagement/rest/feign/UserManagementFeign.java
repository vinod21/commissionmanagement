package com.oasys.commisionmanagement.rest.feign;

import java.util.UUID;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import com.oasys.commisionmanagement.feign.dto.CommonBaseDTO;


@FeignClient(name = "UserManagementFeign", url = "${user.management.host}")
public interface UserManagementFeign {

	String AUTH_TOKEN = "X-Authorization";
	
	@GetMapping("registration/getuser/{id}")
	public CommonBaseDTO getUserById(@RequestHeader(AUTH_TOKEN) String bearerToken, @PathVariable UUID id);
	
	@GetMapping("registration/getByRoleId/{id}")
	public CommonBaseDTO getByRoleId(@RequestHeader(AUTH_TOKEN) String bearerToken, @PathVariable UUID id);

	@GetMapping("usermanagement/get/{id}")
	public CommonBaseDTO getbyId(@RequestHeader(AUTH_TOKEN) String bearerToken, @PathVariable UUID id);


	
}
