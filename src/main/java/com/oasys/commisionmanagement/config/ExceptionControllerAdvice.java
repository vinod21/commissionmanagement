package com.oasys.commisionmanagement.config;

import java.io.IOException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.oasys.commisionmanagement.common.cexception.FeignLayerExcecutionException;
import com.oasys.commisionmanagement.common.cexception.InvalidRequestException;
import com.oasys.commisionmanagement.common.cexception.MissingRequestParam;
import com.oasys.commisionmanagement.common.cexception.NoRecordFoundException;
import com.oasys.commisionmanagement.common.constants.ResponseMessageConstant;
import com.oasys.commisionmanagement.common.dto.CBaseDTO;
import com.oasys.commisionmanagement.feign.dto.FeignAbstractDTO;

import feign.codec.DecodeException;



@ControllerAdvice
public class ExceptionControllerAdvice {


	
	@ExceptionHandler(value = { FeignLayerExcecutionException.class })
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public FeignAbstractDTO exception(HttpServletRequest request, FeignLayerExcecutionException ex) {
		return ex.getBaseDTO();
	}
	
	@ExceptionHandler(value = { DataIntegrityViolationException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public CBaseDTO exception(HttpServletRequest request, DataIntegrityViolationException ex) 
	{
		if(ex.getCause() instanceof ConstraintViolationException)
		{
			return new CBaseDTO(ResponseMessageConstant.DATA_VIOLATION.getErrorCode(),
					ex.getMostSpecificCause().getMessage());
		}
		
		return new CBaseDTO(ResponseMessageConstant.DATA_VIOLATION.getErrorCode(),
				ResponseMessageConstant.DATA_VIOLATION.getMessage());
	}
	
	@ExceptionHandler(value = { AccessDeniedException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public CBaseDTO exception(HttpServletRequest request, AccessDeniedException ex) {
		return new CBaseDTO(ResponseMessageConstant.ACCESS_DENIED.getErrorCode(),
				ResponseMessageConstant.ACCESS_DENIED.getMessage());

	}
	
		
	@ExceptionHandler(value = { NoRecordFoundException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public CBaseDTO exception(HttpServletRequest request, NoRecordFoundException ex) {
		
		if(ex.getResponseMessageConstant()!=null)
		{
			if(ex.getValue()!=null)
			{
				Object[]ob=new Object[1];
				ob[0]=ex.getValue();
				return new CBaseDTO(ex.getResponseMessageConstant().getErrorCode(),
					ex.getResponseMessageConstant().getMessage(ob));
			}
			return new CBaseDTO(ex.getResponseMessageConstant().getErrorCode(),
					ex.getResponseMessageConstant().getMessage());
		}
		
		return new CBaseDTO(ResponseMessageConstant.NO_RECOERD_FOUND.getErrorCode(),
				ResponseMessageConstant.NO_RECOERD_FOUND.getMessage());

	}
	
	@ExceptionHandler(value = { DecodeException.class })

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)

	@ResponseBody
	public FeignAbstractDTO exception(HttpServletRequest request, DecodeException ex) {
		if (ex.getCause() instanceof FeignLayerExcecutionException) {
			return exception(request, (FeignLayerExcecutionException) ex.getCause());
		}
		return exception(request, ex);
	}
	
	@ExceptionHandler(value = {InvalidRequestException.class })
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	public FeignAbstractDTO exception(HttpServletRequest request, InvalidRequestException ex) {
	
		return new CBaseDTO(ResponseMessageConstant.INVALID_REQUEST.getErrorCode(),
				ex.getMessage());
	}
	 
	
	
	

	
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public CBaseDTO handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, Locale locale,
			HttpServletRequest request) {

		BindingResult result = ex.getBindingResult();
		ex.printStackTrace();
		return processFieldErrors(new HashSet<FieldError>(result.getFieldErrors()), locale);

	}

	@ExceptionHandler(MissingRequestParam.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public CBaseDTO handleMethodArgumentNotValidException(MissingRequestParam ex, Locale locale,
			HttpServletRequest request) {
		CBaseDTO commonResponse=new CBaseDTO();
		commonResponse.setStatusCode(ResponseMessageConstant.MANDTORY_REQUEST_PARM.getErrorCode());
		commonResponse.setMessage(ResponseMessageConstant.MANDTORY_REQUEST_PARM.getMessage(new Object[] { ex.getFiledName() }));
		return commonResponse;

	}
	
	@ExceptionHandler(javax.validation.ConstraintViolationException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public CBaseDTO handle(javax.validation.ConstraintViolationException exception, Locale locale) {

		exception.printStackTrace();	
		return processFieldErrors(exception.getConstraintViolations(), locale);
	}
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public CBaseDTO handle(HttpMessageNotReadableException exception, Locale locale) {

		CBaseDTO commonResponse = new CBaseDTO();
		if(exception.getCause() instanceof  InvalidFormatException)
		{
			InvalidFormatException ex=(InvalidFormatException)exception.getCause();
			String field=ex.getPathReference().split("\"")[1];
			commonResponse.setStatusCode(ResponseMessageConstant.INVALID_REQUEST_PARM.getErrorCode());
			commonResponse.setMessage(ResponseMessageConstant.INVALID_REQUEST_PARM.getMessage(new Object[] { field }));
			return commonResponse;
		}
		throw exception;
	}
	private CBaseDTO processFieldErrors(Set<ConstraintViolation<?>> fieldErrors, Locale locale) {
		CBaseDTO commonResponse = new CBaseDTO();
		String defaultMessage = "103";
		String field = "";
		for (ConstraintViolation<?> fieldError : fieldErrors) {

			defaultMessage = fieldError.getMessage();
			field = (String) fieldError.getPropertyPath().toString();

		}
		if (defaultMessage.equalsIgnoreCase("103")) {
			commonResponse.setStatusCode(ResponseMessageConstant.MANDTORY_REQUEST_PARM.getErrorCode());
			commonResponse.setMessage(ResponseMessageConstant.MANDTORY_REQUEST_PARM.getMessage(new Object[] { field }));
			return commonResponse;
		}

		commonResponse.setStatusCode(ResponseMessageConstant.INVALID_REQUEST_PARM.getErrorCode());
		commonResponse.setMessage(ResponseMessageConstant.INVALID_REQUEST_PARM.getMessage(new Object[] { field }));
		return commonResponse;

	}

	private CBaseDTO processFieldErrors(HashSet<FieldError> fieldErrors, Locale locale) {
		CBaseDTO commonResponse = new CBaseDTO();
		String defaultMessage = "103";
		String field = "";

		for (FieldError fieldError : fieldErrors) {
			defaultMessage = fieldError.getDefaultMessage();
			field = fieldError.getField();

		}
		Optional<ResponseMessageConstant> optional=ResponseMessageConstant.getResponseMessageContext(Integer.parseInt(defaultMessage));
		if(optional.isPresent())
		{
			
			commonResponse.setStatusCode(optional.get().getErrorCode());
			commonResponse.setMessage(optional.get().getMessage(new Object[] { field }));
			return commonResponse;
		}
		
		commonResponse.setStatusCode(ResponseMessageConstant.INVALID_REQUEST_PARM.getErrorCode());
		commonResponse.setMessage(ResponseMessageConstant.INVALID_REQUEST_PARM.getMessage(new Object[] { field }));
		return commonResponse;
	}

	@ExceptionHandler(value = { IOException.class, Exception.class })
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public CBaseDTO exception(HttpServletRequest request, Exception ex) {
		
		ex.printStackTrace();
		return new CBaseDTO(ResponseMessageConstant.INTERNAL_SERVER_ERROR.getErrorCode(),
				ResponseMessageConstant.INTERNAL_SERVER_ERROR.getMessage());
	}
}