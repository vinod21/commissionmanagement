package com.oasys.commisionmanagement.common.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;


@Target({ElementType.FIELD,ElementType.ANNOTATION_TYPE,ElementType.METHOD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NumberLengthValidator.class)
@Documented
public @interface NumberLength 
{
	

		int min();
		int max();
	    String message() default "Invalid Number";

	    Class<?>[] groups() default { };

	    Class<? extends Payload>[] payload() default { }; 
	    
	    @Target(ElementType.TYPE)
	    @Retention(RetentionPolicy.RUNTIME)
	    @Documented
	    @interface List {
	        NumberLength[] value();
	    }
	
}
