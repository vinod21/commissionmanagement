package com.oasys.commisionmanagement.common.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Repeatable(ValidateBasedOnAnotherFieldValue.List.class) 
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidateBasedOnAnotherFieldValueImpl.class)
@Documented
public @interface ValidateBasedOnAnotherFieldValue 
{
	

		String fieldName();
	    String[]  fieldValue();
	    String dependFieldName();
	  
	    String message();

	    Class<?>[] groups() default { };

	    Class<? extends Payload>[] payload() default { }; 
	    
	    @Target(ElementType.TYPE)
	    @Retention(RetentionPolicy.RUNTIME)
	    @Documented
	    @interface List {
	        ValidateBasedOnAnotherFieldValue[] value();
	    }
	
}
