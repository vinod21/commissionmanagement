package com.oasys.commisionmanagement.common.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Repeatable(NotEmptyIfAnotherFieldHasValue.List.class) 
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotEmptyIfAnotherFieldHasValueImpl.class)
@Documented
public @interface NotEmptyIfAnotherFieldHasValue 
{
	

		String fieldName();
	    String fieldValue();
	    String dependFieldName();
		
	    String message() default "103";

	    Class<?>[] groups() default { };

	    Class<? extends Payload>[] payload() default { }; 
	    
	    @Target(ElementType.TYPE)
	    @Retention(RetentionPolicy.RUNTIME)
	    @Documented
	    @interface List {
	        NotEmptyIfAnotherFieldHasValue[] value();
	    }
	
}
