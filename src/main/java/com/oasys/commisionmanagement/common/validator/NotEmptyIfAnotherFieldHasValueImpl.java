package com.oasys.commisionmanagement.common.validator;

import java.util.Collection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.oasys.commisionmanagement.common.cexception.ServiceLayerExecutionException;
import com.oasys.commisionmanagement.common.utils.CommonUtil;

public class NotEmptyIfAnotherFieldHasValueImpl implements ConstraintValidator<NotEmptyIfAnotherFieldHasValue, Object> {

	private String fieldName;
	private String expectedFieldValue;
	private String dependFieldName;

	@Override
	public void initialize(NotEmptyIfAnotherFieldHasValue notEmptyIfAnotherFieldHasValue) {
		fieldName = notEmptyIfAnotherFieldHasValue.fieldName();
		expectedFieldValue = notEmptyIfAnotherFieldHasValue.fieldValue();
		dependFieldName = notEmptyIfAnotherFieldHasValue.dependFieldName();

	}

	public boolean isValid(Object value, ConstraintValidatorContext constraintContext) {

		if (value == null) {
			return true;
		}
		try {
			String fieldNameValue = (String) CommonUtil.getFiledValue(value, fieldName);
			if (fieldNameValue != null && fieldNameValue.equalsIgnoreCase(expectedFieldValue)) {
				Object dependFiledValue = CommonUtil.getFiledValue(value, dependFieldName);
				if (dependFiledValue == null) {
					return false;
				}
				if (dependFiledValue instanceof String) {
					return ((String) dependFiledValue).length() != 0;
				}
				if (dependFiledValue instanceof Collection<?>) {
					return ((Collection<?>) dependFiledValue).size() != 0;
				}
				return true;
			}
			return true;
		} catch (Exception e) {

			throw new ServiceLayerExecutionException("Exception in validate NotEmptyIfAnotherFieldHasValueImpl", e);
		}

	}

}
