package com.oasys.commisionmanagement.common.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;


@Target({ElementType.FIELD,ElementType.ANNOTATION_TYPE,ElementType.METHOD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StringValidatorImpl.class)
@Documented
public @interface ValidateString 
{
	

	    String[] acceptedValues();
	    boolean ignoreCase();
	    boolean isAllowNull();
	    String message() default "103";

	    Class<?>[] groups() default { };

	    Class<? extends Payload>[] payload() default { }; 
	
}
