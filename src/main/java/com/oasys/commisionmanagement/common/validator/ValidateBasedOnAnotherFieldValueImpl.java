package com.oasys.commisionmanagement.common.validator;

import java.util.Set;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import com.oasys.commisionmanagement.common.cexception.ServiceLayerExecutionException;
import com.oasys.commisionmanagement.common.utils.CommonUtil;

public class ValidateBasedOnAnotherFieldValueImpl
		implements ConstraintValidator<ValidateBasedOnAnotherFieldValue, Object> {

	@Resource
	private Validator validator;

	private String fieldName;
	private String []expectedFieldValue;
	private String dependFieldName;


	@Override
	public void initialize(ValidateBasedOnAnotherFieldValue validateBasedOnAnotherFieldValue) {
		fieldName = validateBasedOnAnotherFieldValue.fieldName();
		expectedFieldValue = validateBasedOnAnotherFieldValue.fieldValue();
		dependFieldName = validateBasedOnAnotherFieldValue.dependFieldName();
	}

	public boolean isValid(Object value, ConstraintValidatorContext constraintContext) {

		if (value == null) {
			return true;
		}
		
		Object dependFieldNameValue = getDefiendNameValue(value);
		if(dependFieldNameValue==null)
		{
			return false;
		}
		if(dependFieldNameValue instanceof Boolean)
		{
			return true;
		}
		Set<ConstraintViolation<Object>> cvs = validator.validate(dependFieldNameValue);
		if (!cvs.isEmpty())
			throw new ConstraintViolationException(cvs);
		return true;

	}
	private Object getDefiendNameValue(Object value)
	{
		try {
			String fieldNameValue = CommonUtil.getFiledValue(value, fieldName).toString();
			
			for(String expectedField: expectedFieldValue)
			{
				if (fieldNameValue != null && fieldNameValue.equalsIgnoreCase(expectedField)) {
	
					return  (Object) CommonUtil.getFiledValue(value, dependFieldName);
				}
			}	
			return true;

		} catch (Exception e) {

			throw new ServiceLayerExecutionException("Exception in validate ValidateBasedOnAnotherFieldValueImpl", e);
		}
	}

}
