package com.oasys.commisionmanagement.common.validator;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import lombok.val;

public class StringValidatorImpl implements ConstraintValidator<ValidateString, String>
{

    private List<String> valueList;
    private boolean eqalIgnoreCase;
    private boolean isAllowNull;

    @Override
    public void initialize(ValidateString constraintAnnotation) {
        valueList = new ArrayList<>();
        for(String val : constraintAnnotation.acceptedValues()) {
            valueList.add(val);
        }
        
        eqalIgnoreCase=constraintAnnotation.ignoreCase();
        isAllowNull=constraintAnnotation.isAllowNull();
    }

    @Override
    public boolean isValid(final String value, ConstraintValidatorContext context)
    {
    	
    	
    	if(value==null&&isAllowNull)
    	{
    		return true;
    	}
    	if(value==null)
    	{
    		return false;
    	}
    	
    	if(eqalIgnoreCase)
    	{	
    		
    		return valueList.stream().anyMatch(s->s.equalsIgnoreCase(value));
    	}
    	else
    	{
    		return valueList.stream().anyMatch(s->s.equals(value));
    	}
        
    }

}
