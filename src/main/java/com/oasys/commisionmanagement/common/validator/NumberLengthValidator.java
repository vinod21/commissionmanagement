package com.oasys.commisionmanagement.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public  class NumberLengthValidator implements ConstraintValidator<NumberLength, Number> 
{

    private int min;
    private int max;

    @Override
    public void initialize(NumberLength fieldMatch) {
        min = fieldMatch.min();
        max = fieldMatch.max();
    }

    public boolean isValid(Number number, ConstraintValidatorContext constraintContext) 
    {
        
            int length=number.toString().length();
            
            if(min<=length&&length<=max)
            {
            	return true;
            }
            
            return false;
         
        
    }

    
}
