package com.oasys.commisionmanagement.common.utils;

	import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

	@Component
	public class DateUtill {
		
		public Date fromDateFormat(String date) {
			Date newDate = new Date();
			String oldstring = date + " 00:00:00";
			try {
				newDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(oldstring);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return newDate;
		}

		public Date toDateFormat(String date) {
			Date newDate = new Date();
			String oldstring = date + " 23:59:59";
			try {
				newDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(oldstring);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return newDate;
		}
		
		
		public static  Date fromDateFormat(Date date) {
			
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			return calendar.getTime();
			
			
		}

		public static Date toDateFormat(Date date) {
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(date);
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			return calendar.getTime();
			
		}
		
		

}
