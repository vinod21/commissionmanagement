package com.oasys.commisionmanagement.common.utils;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oasys.commisionmanagement.common.cexception.ServiceLayerExecutionException;
import com.oasys.commisionmanagement.common.constants.ChargingType;

@Component
public class CommonUtil {
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private ObjectMapper objectMapper;

	public <T> T modalMap(Object ob, Class<T> type) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		return modelMapper.map(ob, type);

	}

	public <S, T> List<T> modalMap(Collection<S> ob, Class<T> type) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		return ob.stream().map(element -> modelMapper.map(element, type)).collect(Collectors.toList());

	}

	public void modalMapCopy(Object source, Object destination) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		modelMapper.map(source, destination);

	}

	public <T> T ojectMap(Object ob, Class<T> type) {
		return objectMapper.convertValue(ob, type);

	}

	public <T> T jsonToObject(String json, Class<T> type) {
		try {
			return objectMapper.readValue(json, type);
		} catch (Exception e) {
			throw new ServiceLayerExecutionException(e);
		}
	}

	public static Object getFiledValue(Object value, String filedKey)
			throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		String key = filedKey.split("\\.")[0];
		Field field1 = value.getClass().getDeclaredField(key); //
		field1.setAccessible(true);
		Object getValue = field1.get(value);
		if (getValue == null || !filedKey.contains(".")) {
			return getValue;
		}
		return getFiledValue(getValue, filedKey.substring(filedKey.indexOf(".") + 1, filedKey.length()));
	}

	public String getNewVesrion() {
		int number = 1;
		return String.format("%02d", number);

	}

	public String getNewVesrion(String currentVersion) {
		int number = 1;
		if (currentVersion != null) {
			number = Integer.parseInt(currentVersion);
		}
		return String.format("%02d", ++number);

	}

	public Calendar getCurrentDate() {
		return Calendar.getInstance();
	}

	public Long genrateUniqueId() {
		return Calendar.getInstance().getTimeInMillis();
	}

	public static Double getPercantageValue(Double value, Double percantage) {
		return (value * percantage) / 100;
	}

	public static Double getPercantageValue(Double value, Double percantage, ChargingType chargingType) {
		if (ChargingType.FLAT == chargingType)
			return percantage;
		return (value * percantage) / 100;
	}
	
	

}
