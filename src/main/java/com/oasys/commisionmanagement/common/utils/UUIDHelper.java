package com.oasys.commisionmanagement.common.utils;

import java.util.UUID;

import org.springframework.stereotype.Service;

@Service
public interface UUIDHelper {

	public UUID convertToUUID(UUID s);
}
