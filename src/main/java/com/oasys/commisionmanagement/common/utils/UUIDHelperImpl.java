package com.oasys.commisionmanagement.common.utils;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UUIDHelperImpl implements UUIDHelper {

	@Autowired
	ObjectMapper mapper;

	public UUID convertToUUID(UUID s) {

		UUID convertValue = mapper.convertValue(s, UUID.class);

		return convertValue;

	}

}
