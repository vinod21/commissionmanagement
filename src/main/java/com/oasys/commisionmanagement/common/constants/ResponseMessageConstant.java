package com.oasys.commisionmanagement.common.constants;

import java.util.Locale;
import java.util.Optional;

import org.springframework.context.MessageSource;

public enum ResponseMessageConstant {
	
	SUCCESS_RESPONSE(200,200),
	CREATE_SUCCESS_RESPONSE(200,201),
	DELET_RESPONSE(200,202),
	UPDAE_RESPONSE(200,203),
	SEARCH_RESPONSE(200,204),
	CREATED(201,201),
	NON_AUTHORITATIVE_INFORMATION(203,203),
	EMPTY_DATA(204,2014),
	ACCESS_DENIED(401,401),
	AUTHENTICATION_FAILED(401,127),
	INVALID_USER(417,126),
	BAD_CREDENTIAL(204,127),
	INTERNAL_SERVER_ERROR(500,500),
	ALREADY_EXISTS(409,409),
	INVALID_REQUEST_WITH_ATRIBUTE(400,400),
	INVALID_REQUEST(400,401),
	WORK_FLOW_NOT_CONFIGUERD(400,435),
	DATA_VIOLATION_WITH_ATRIBUTE(409,409),
	DATA_VIOLATION(409,410),
	NO_RECOERD_FOUND(404,125),
	INVALID_REQUEST_PARM(400,105),
	MANDTORY_REQUEST_PARM(400,103),
	INVALID_REQUEST_PARM_POSTIVE_NUMBER_ONLY(400,106),
	REQUEST_ALREADY_PROCESSD(409,436),
	REQUEST_ALREADY_PROCESSD_WITH_ATTRIBUTE(409,437),
	INVALID_ACCESS_APPROVAL(401,438),
	MUST_BE_SAME_STATE(400,439),
	MUST_BE_SAME_DISTRICT(400,440),
	MUST_BE_SAME_PINCODE(400,441),
	IP_ACCEES_DENIED(401,442),
	PLAN_NOT_ACTIVE(400,443),
	PLAN_NOT_ACTIVE_FOR_ROLE(400,444),
	PLAN_NOT_ACTIVE_FOR_USER(400,444);
	private int errorCode;
	private int messageCode;
	
	private ResponseMessageConstant(int errorCode,int messageCode) {
		this.errorCode=errorCode;
		this.messageCode=messageCode;
	}
	private MessageSource messageSource;
	
	public int getErrorCode() {
		return errorCode;
	}
	
	
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	public String getMessage() 
	{		
		return messageSource.getMessage(String.valueOf(messageCode),new Object[]{},Locale.ENGLISH);
	}
	public String getMessage(Object[] arg1) {
		return messageSource.getMessage(String.valueOf(messageCode), arg1, Locale.ENGLISH);
	}
	
	public String getMessage(String value) {
		Object[]ob=new Object[1];
		ob[0]=value;
		return messageSource.getMessage(String.valueOf(messageCode), ob, Locale.ENGLISH);
	}
	public static Optional<ResponseMessageConstant> getResponseMessageContext(int code)
	{
		for(ResponseMessageConstant responseMessageConstant:ResponseMessageConstant.values())
		{
				if(responseMessageConstant.messageCode==code)
				{
					return  Optional.of(responseMessageConstant);
				}	
		}
		return Optional.of(null);
		
	}
}
