package com.oasys.commisionmanagement.common.constants;

public enum SettlementPeriod {

	INSTANT("INSTANT",1),MONTHLY("MONTHLY",2);
	
	private SettlementPeriod(String type,int id)
	{
		this.type=type;
		this.id=id;
		
    }
	String type;
	int id;
	public String getType() {
		return type;
	}
	public int getId() {
		return id;
	}
}
