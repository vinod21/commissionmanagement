package com.oasys.commisionmanagement.common.constants;

public enum ChargingType {

	
	FLAT("FLAT",1),PERCENTAGE("PERCENTAGE",2);
	
	private ChargingType(String type,int id)
	{
		this.type=type;
		this.id=id;
		
    }
	String type;
	int id;
	public String getType() {
		return type;
	}
	public int getId() {
		return id;
	}
	
}
