package com.oasys.commisionmanagement.common.constants;

public enum HierarchyType {
	
	ADPR("ADPR",1),ADR("ADR",2),APR("APR",3),AR("AR",4);
	
	private HierarchyType(String type,int id)
	{
		this.type=type;
		this.id=id;
		
    }
	String type;
	int id;
	public String getType() {
		return type;
	}
	public int getId() {
		return id;
	}

}
