package com.oasys.commisionmanagement.common.constants;

public enum OrderConstant {
	ASC("ASC"), DESC("DESC");

	private String value;

	OrderConstant(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	
}
