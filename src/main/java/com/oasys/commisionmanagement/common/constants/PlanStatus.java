package com.oasys.commisionmanagement.common.constants;

public enum PlanStatus {

	
	ACTIVE("ACTIVE",1),INACTIVE("INACTIVE",2),SAVE("SAVE",3);
	
	private PlanStatus(String type,int id)
	{
		this.type=type;
		this.id=id;
		
    }
	String type;
	int id;
	public String getType() {
		return type;
	}
	public int getId() {
		return id;
	}
	
}
