package com.oasys.commisionmanagement.common.constants;


public enum PlanType {

	ASDPR("ASDPR"),ASPR("ASPR"),ASDR("ASDR"),ADPR("ADPR"),ASR("ASR"),ADR("ADR"),APR("APR"),AR("AR");
	
	private String planType;

	private PlanType(String planType) {
		this.planType = planType;
	}

	public String getplanType() {
		return planType;
	}
}
