package com.oasys.commisionmanagement.common.constants;

public enum ServiceCharge {

	SERVICECHARGE("Service Charge",1),COMMISSION("Commission",2);
	
	private ServiceCharge(String type,int id)
	{
		this.type=type;
		this.id=id;
		
    }
	String type;
	int id;
	public String getType() {
		return type;
	}
	public int getId() {
		return id;
	}
}
