package com.oasys.commisionmanagement.common.constants;

public enum GSTType {

	EXCLUSIVE("EXCLUSIVE",1),INCLUSIVE("INCLUSIVE",2);
	
	private GSTType(String type,int id)
	{
		this.type=type;
		this.id=id;
		
    }
	String type;
	int id;
	public String getType() {
		return type;
	}
	public int getId() {
		return id;
	}
}
