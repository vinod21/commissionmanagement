package com.oasys.commisionmanagement.common.cexception;


public class OldCredantialException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public OldCredantialException() {
		super();
	
	}


	public OldCredantialException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public OldCredantialException(String message) {
		super(message);
	
	}

	public OldCredantialException(Throwable cause) {
		super(cause);
	
	}
	
}
