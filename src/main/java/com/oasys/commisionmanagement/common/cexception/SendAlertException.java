package com.oasys.commisionmanagement.common.cexception;


public class SendAlertException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;
	
	private final Long userId;
	private final  String apiId;
	private final String taskName;

	public SendAlertException(String message,Long userId, String apiId,String taskName,Throwable cause) {
		super(message, cause);
		this.userId = userId;
		this.apiId = apiId;
		this.taskName=taskName;
		
	}
	public Long getUserId() {
		return userId;
	}
	public String getApiId() {
		return apiId;
	}

	public String getTaskName() {
		return taskName;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
