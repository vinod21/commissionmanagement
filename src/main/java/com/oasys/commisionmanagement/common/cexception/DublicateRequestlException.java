package com.oasys.commisionmanagement.common.cexception;

import java.io.IOException;

public class DublicateRequestlException extends IOException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public DublicateRequestlException() {
		super();
	
	}


	public DublicateRequestlException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public DublicateRequestlException(String message) {
		super(message);
	
	}

	public DublicateRequestlException(Throwable cause) {
		super(cause);
	
	}
	
}
