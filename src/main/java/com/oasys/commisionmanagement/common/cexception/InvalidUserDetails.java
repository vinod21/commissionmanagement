package com.oasys.commisionmanagement.common.cexception;

public class InvalidUserDetails extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public InvalidUserDetails() {
		super();
	
	}

	public InvalidUserDetails(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	
	}

	public InvalidUserDetails(String message, Throwable cause) {
		super(message, cause);
	
	}

	public InvalidUserDetails(String message) {
		super(message);
	
	}

	public InvalidUserDetails(Throwable cause) {
		super(cause);
	
	}
	
}
