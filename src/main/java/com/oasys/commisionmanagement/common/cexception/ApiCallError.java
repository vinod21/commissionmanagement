package com.oasys.commisionmanagement.common.cexception;


public class ApiCallError extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public ApiCallError() {
		super();
	
	}

	

	public ApiCallError(String message, Throwable cause) {
		super(message, cause);
	
	}

	public ApiCallError(String message) {
		super(message);
	
	}

	public ApiCallError(Throwable cause) {
		super(cause);
	
	}
	
}
