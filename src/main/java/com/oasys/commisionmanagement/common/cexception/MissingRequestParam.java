package com.oasys.commisionmanagement.common.cexception;

import com.oasys.commisionmanagement.common.constants.ResponseMessageConstant;

public class MissingRequestParam extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;
	
	private String filedName;

	public MissingRequestParam(String filedName)
	{
		super(ResponseMessageConstant.MANDTORY_REQUEST_PARM.getMessage(new Object[]{filedName}));
		this.filedName=filedName;
	}

	public String getFiledName() {
		return filedName;
	}

	
}
