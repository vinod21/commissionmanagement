package com.oasys.commisionmanagement.common.cexception;

import java.io.IOException;

public class InvalidCredantialException extends IOException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public InvalidCredantialException() {
		super();
	
	}


	public InvalidCredantialException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public InvalidCredantialException(String message) {
		super(message);
	
	}

	public InvalidCredantialException(Throwable cause) {
		super(cause);
	
	}
	
}
