package com.oasys.commisionmanagement.common.cexception;

public class UserBlockException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public UserBlockException() {
		super();
	
	}

	public UserBlockException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	
	}

	public UserBlockException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public UserBlockException(String message) {
		super(message);
	
	}

	public UserBlockException(Throwable cause) {
		super(cause);
	
	}
	
}
