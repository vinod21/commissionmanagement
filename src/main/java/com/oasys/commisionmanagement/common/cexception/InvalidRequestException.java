package com.oasys.commisionmanagement.common.cexception;

import com.oasys.commisionmanagement.common.constants.ResponseMessageConstant;

public class InvalidRequestException  extends RuntimeException{

	/**
	 * 
	 */
	private  Object value;
	private static final long serialVersionUID = 3598859628828600953L;
	private ResponseMessageConstant responseMessageConstant;
    
	public InvalidRequestException()
	{
		
	}
	public InvalidRequestException(ResponseMessageConstant responseMessageConstant) {
		super();
		this.responseMessageConstant = responseMessageConstant;
	}

	public InvalidRequestException(ResponseMessageConstant responseMessageConstant,Throwable cause) {
		super(cause);
		this.responseMessageConstant = responseMessageConstant;
		
	}
	
	public InvalidRequestException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public InvalidRequestException(String message) {
		super(message);
	
	}

	public InvalidRequestException(Throwable cause) {
		super(cause);
	}


	public Object getValue() {
		return value;
	}


	public ResponseMessageConstant getResponseMessageConstant() {
		return responseMessageConstant;
	}

	
	
}
