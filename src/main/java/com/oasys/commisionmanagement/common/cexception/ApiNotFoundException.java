package com.oasys.commisionmanagement.common.cexception;

import java.io.IOException;

public class ApiNotFoundException extends IOException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public ApiNotFoundException() {
		super();
	
	}

	

	public ApiNotFoundException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public ApiNotFoundException(String message) {
		super(message);
	
	}

	public ApiNotFoundException(Throwable cause) {
		super(cause);
	
	}
	
}
