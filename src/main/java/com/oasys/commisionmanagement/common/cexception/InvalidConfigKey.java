package com.oasys.commisionmanagement.common.cexception;

public class InvalidConfigKey extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public InvalidConfigKey() {
		super();
	
	}

	public InvalidConfigKey(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	
	}

	public InvalidConfigKey(String message, Throwable cause) {
		super(message, cause);
	
	}

	public InvalidConfigKey(String message) {
		super(message);
	
	}

	public InvalidConfigKey(Throwable cause) {
		super(cause);
	
	}
	
}
