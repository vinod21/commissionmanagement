package com.oasys.commisionmanagement.common.cexception;

public class InvalidTenantIdException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public InvalidTenantIdException() {
		super();
	
	}

	public InvalidTenantIdException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	
	}

	public InvalidTenantIdException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public InvalidTenantIdException(String message) {
		super(message);
	
	}

	public InvalidTenantIdException(Throwable cause) {
		super(cause);
	
	}
	
}
