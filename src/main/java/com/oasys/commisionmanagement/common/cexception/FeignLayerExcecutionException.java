package com.oasys.commisionmanagement.common.cexception;

import com.oasys.commisionmanagement.feign.dto.FeignAbstractDTO;

public class FeignLayerExcecutionException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;
	private FeignAbstractDTO baseDTO;
	

	public FeignLayerExcecutionException() {
		super();
	
	}
	
	public FeignLayerExcecutionException(String message,FeignAbstractDTO baseDTO) {
		super(message);
		this.baseDTO = baseDTO;
	}

	public FeignLayerExcecutionException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	
	}

	public FeignLayerExcecutionException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public FeignLayerExcecutionException(String message) {
		super(message);
	
	}

	public FeignLayerExcecutionException(Throwable cause) {
		super(cause);
	
	}

	public FeignAbstractDTO getBaseDTO() {
		return baseDTO;
	}

}
