package com.oasys.commisionmanagement.common.cexception;


public class InvalidIPException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public InvalidIPException() {
		super();
	
	}

	

	public InvalidIPException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public InvalidIPException(String message) {
		super(message);
	
	}

	public InvalidIPException(Throwable cause) {
		super(cause);
	
	}
	
}
