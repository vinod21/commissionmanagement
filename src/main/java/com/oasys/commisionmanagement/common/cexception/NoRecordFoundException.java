package com.oasys.commisionmanagement.common.cexception;

import com.oasys.commisionmanagement.common.constants.ResponseMessageConstant;

public class NoRecordFoundException  extends RuntimeException{

	/**
	 * 
	 */
	private  Object value;
	private static final long serialVersionUID = 3598859628828600953L;
	private ResponseMessageConstant responseMessageConstant;
    
	public NoRecordFoundException()
	{
		
	}
	public NoRecordFoundException(ResponseMessageConstant responseMessageConstant) {
		super();
		this.responseMessageConstant = responseMessageConstant;
	}

	public NoRecordFoundException(ResponseMessageConstant responseMessageConstant,Throwable cause) {
		super(cause);
		this.responseMessageConstant = responseMessageConstant;
		
	}
	
	public NoRecordFoundException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public NoRecordFoundException(String message) {
		super(message);
	
	}

	public NoRecordFoundException(Throwable cause) {
		super(cause);
	}


	public Object getValue() {
		return value;
	}


	public ResponseMessageConstant getResponseMessageConstant() {
		return responseMessageConstant;
	}

	
	
}
