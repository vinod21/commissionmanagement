package com.oasys.commisionmanagement.common.cexception;

public class SslDisableException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public SslDisableException() {
		super();
	
	}

	public SslDisableException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	
	}

	public SslDisableException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public SslDisableException(String message) {
		super(message);
	
	}

	public SslDisableException(Throwable cause) {
		super(cause);
	
	}
	
}
