package com.oasys.commisionmanagement.common.cexception;

public class DublicateUserException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public DublicateUserException() {
		super();
	
	}

	public DublicateUserException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	
	}

	public DublicateUserException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public DublicateUserException(String message) {
		super(message);
	
	}

	public DublicateUserException(Throwable cause) {
		super(cause);
	
	}
	
}
