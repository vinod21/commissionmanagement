package com.oasys.commisionmanagement.common.cexception;

public class UserDeleteException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3598859628828600953L;

	public UserDeleteException() {
		super();
	
	}

	public UserDeleteException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	
	}

	public UserDeleteException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public UserDeleteException(String message) {
		super(message);
	
	}

	public UserDeleteException(Throwable cause) {
		super(cause);
	
	}
	
}
