package com.oasys.commisionmanagement.common.dto;

import lombok.Data;

@Data
public class CalculateCommissionsDTO {

	String type;
	Object globalCommission;  
	Object customCommission; 
}
