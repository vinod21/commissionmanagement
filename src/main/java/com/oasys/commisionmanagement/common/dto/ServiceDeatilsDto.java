package com.oasys.commisionmanagement.common.dto;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceDeatilsDto {

	@NotNull(message = "103")
	private UUID serviceId;
	
	@NotNull(message = "103")
	private UUID serviceProviderId;
	
	@NotNull(message = "103")
	private UUID slabId;
	
	
}
