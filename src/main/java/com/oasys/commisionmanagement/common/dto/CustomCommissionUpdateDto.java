package com.oasys.commisionmanagement.common.dto;

import java.util.Calendar;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.oasys.commisionmanagement.common.constants.ChargingType;
import com.oasys.commisionmanagement.common.constants.GSTType;
import com.oasys.commisionmanagement.common.constants.ServiceCharge;
import com.oasys.commisionmanagement.common.constants.SettlementPeriod;

import lombok.Data;

@Data
public class CustomCommissionUpdateDto  {


	@NotNull(message = "103")
	private UUID customCommissionId;

	@NotNull(message = "103")
	private Double adminCommisionValue;
	
	private Double ddCommisionValue;
	
	private Double pcdCommisionValue;
	
	private Double rCommisionValue;

	private ChargingType chargingType;	
	
	@NotNull(message = "103")
	private ServiceCharge serviceCharge;
	
	@NotNull(message = "103")
	private GSTType gstType;
	
	@NotNull(message = "103")
	private boolean tdsApplicable;
	
	@NotNull(message = "103")
	private SettlementPeriod settlementPeriod;
	
	@NotNull(message = "103")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Calendar  fromDate;
	
	@NotNull(message = "103")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Kolkata")
	private Calendar  toDate;
	
}
