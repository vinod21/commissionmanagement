package com.oasys.commisionmanagement.common.dto;

import java.util.UUID;

import javax.validation.constraints.Min;

import com.oasys.commisionmanagement.common.constants.HierarchyType;

import lombok.Data;

@Data
public class CommissionFilterDto {

	private HierarchyType hierarchyTypes;
	private UUID serviceId;
	private UUID agentId;
	private UUID recordId;
	@Min(value =0,message = "pageNo must be equals or greater then 0")
	private int pageNo;
	@Min(value =10,message = "pageSize must be equals or greater then 10")
	private int pageSize;

}
