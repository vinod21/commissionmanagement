package com.oasys.commisionmanagement.common.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.oasys.commisionmanagement.common.constants.ResponseMessageConstant;
import com.oasys.commisionmanagement.feign.dto.FeignAbstractDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CBaseDTO extends FeignAbstractDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	Integer statusCode;

	String message;

	Object responseContent;

	List<?> responseContents;
	
	Integer pageNo;
	
	Integer PageSize;
	
	Long totalRecords;
	
	Integer NumberOfElements;
	
	Integer totalPages;
	
	public CBaseDTO() {
		
		statusCode = ResponseMessageConstant.SUCCESS_RESPONSE.getErrorCode();
		message = ResponseMessageConstant.SUCCESS_RESPONSE.getMessage();
	}
	public CBaseDTO(ResponseMessageConstant responseMessageConstant) {
		
		statusCode = responseMessageConstant.getErrorCode();
		message = responseMessageConstant.getMessage();
	}

	public CBaseDTO(Integer statusCode, String message) {
		this.statusCode = statusCode;
		this.message = message;
	}
	
}
