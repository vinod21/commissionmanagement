package com.oasys.commisionmanagement.common.dto;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import com.oasys.commisionmanagement.common.constants.ChargingType;
import com.oasys.commisionmanagement.common.constants.GSTType;
import com.oasys.commisionmanagement.common.constants.ServiceCharge;
import com.oasys.commisionmanagement.common.constants.SettlementPeriod;

import lombok.Data;

@Data
public class GlobalCommissionUpdateDto  {


	@NotNull(message = "103")
	private UUID globalCommissionId;

	@NotNull(message = "103")
	private Double adminCommisionValue;
	
	private Double ddCommisionValue;
	
	private Double pcdCommisionValue;
	
	private Double rCommisionValue;
	
	@NotNull(message = "103")
	private ChargingType chargingType;
	
	@NotNull(message = "103")
	private ServiceCharge serviceCharge;
	
	@NotNull(message = "103")
	private GSTType gstType;
	
	@NotNull(message = "103")
	private boolean tdsApplicable;
	
	@NotNull(message = "103")
	private SettlementPeriod settlementPeriod;
	
}
