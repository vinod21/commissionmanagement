package com.oasys.commisionmanagement.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.oasys.commisionmanagement.common.cexception.NoRecordFoundException;
import com.oasys.commisionmanagement.common.dto.CommissionFilterDto;
import com.oasys.commisionmanagement.entity.CustomCommissionEntity;
import com.oasys.commisionmanagement.entity.GlobalCommissionSettingEntity;

@Repository
public class CommonCommissionRepositoryImpl {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	
	public List<GlobalCommissionSettingEntity> serachGlobalCommissionSetting(CommissionFilterDto commissionFilterDto) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<GlobalCommissionSettingEntity> cq = cb.createQuery(GlobalCommissionSettingEntity.class);
		Root<GlobalCommissionSettingEntity> from = cq.from(GlobalCommissionSettingEntity.class);

		List<Predicate> conList = new ArrayList<>();

		if (commissionFilterDto.getHierarchyTypes() != null) {
			conList.add(cb.equal(from.get("hierarchyType"),commissionFilterDto.getHierarchyTypes()));
		}
		if (commissionFilterDto.getServiceId() != null) {

			conList.add(cb.equal(from.get("serviceId"), commissionFilterDto.getServiceId()));
		}
		
		if (commissionFilterDto.getRecordId() != null) {

			conList.add(cb.equal(from.get("globalCommissionId"), commissionFilterDto.getRecordId()));
		}

		cq.where(cb.and(conList.toArray(new Predicate[conList.size()])));
		cq.distinct(true);
		cq.orderBy(cb.asc(from.get("hierarchyType")));

		TypedQuery<GlobalCommissionSettingEntity> typedQuery = entityManager.createQuery(cq)
				.setFirstResult(commissionFilterDto.getPageNo())
				.setMaxResults(commissionFilterDto.getPageSize());

		List<GlobalCommissionSettingEntity> data = typedQuery.getResultList();
		if (data == null || data.isEmpty()) {
			throw new NoRecordFoundException();
		}
		return data;
	}
	
	public List<CustomCommissionEntity> serachCustomCommission(CommissionFilterDto commissionFilterDto) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CustomCommissionEntity> cq = cb.createQuery(CustomCommissionEntity.class);
		Root<CustomCommissionEntity> from = cq.from(CustomCommissionEntity.class);

		List<Predicate> conList = new ArrayList<>();

		if (commissionFilterDto.getHierarchyTypes() != null) {
			conList.add(cb.equal(from.get("hierarchyType"),commissionFilterDto.getHierarchyTypes()));
		}
		if (commissionFilterDto.getServiceId() != null) {

			conList.add(cb.equal(from.get("serviceId"), commissionFilterDto.getServiceId()));
		}
		
		if (commissionFilterDto.getRecordId() != null) {

			conList.add(cb.equal(from.get("customCommissionId"), commissionFilterDto.getRecordId()));
		}
		
		if (commissionFilterDto.getAgentId() != null) {

			conList.add(cb.equal(from.get("agentId"), commissionFilterDto.getAgentId()));
		}


		cq.where(cb.and(conList.toArray(new Predicate[conList.size()])));
		cq.distinct(true);
		cq.orderBy(cb.asc(from.get("agentId")));

		TypedQuery<CustomCommissionEntity> typedQuery = entityManager.createQuery(cq)
				.setFirstResult(commissionFilterDto.getPageNo())
				.setMaxResults(commissionFilterDto.getPageSize());

		List<CustomCommissionEntity> data = typedQuery.getResultList();
		if (data == null || data.isEmpty()) {
			throw new NoRecordFoundException();
		}
		return data;
	}

}
