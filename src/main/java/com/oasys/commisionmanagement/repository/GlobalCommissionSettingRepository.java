package com.oasys.commisionmanagement.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oasys.commisionmanagement.common.constants.HierarchyType;
import com.oasys.commisionmanagement.entity.GlobalCommissionSettingEntity;

public interface GlobalCommissionSettingRepository extends JpaRepository<GlobalCommissionSettingEntity, UUID> {

	@Query("SELECT obj  FROM GlobalCommissionSettingEntity obj "
			+ "WHERE obj.hierarchyType = :hierarchy and obj.serviceId=:serviceId and "
			+ "obj.serviceProviderId=:serviceProviderId  and obj.slabId = :slabId ")
	Optional<GlobalCommissionSettingEntity> findActiveCommission(@Param("hierarchy") HierarchyType hierarchyType,
			@Param("serviceId") UUID serviceId, @Param("serviceProviderId") UUID serviceProviderId,
			@Param("slabId") UUID slabId);

}
