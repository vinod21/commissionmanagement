package com.oasys.commisionmanagement.repository;

import java.util.Calendar;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oasys.commisionmanagement.common.constants.HierarchyType;
import com.oasys.commisionmanagement.entity.CustomCommissionEntity;

public interface CustomCommissionRepository extends JpaRepository<CustomCommissionEntity, UUID> {

	@Query("SELECT obj  FROM CustomCommissionEntity obj "
			+ "WHERE obj.hierarchyType = :hierarchy and obj.serviceId=:serviceId and obj.agentId=:agentId and "
			+ "obj.serviceProviderId=:serviceProviderId and obj.slabId = :slabId ")
	Optional<CustomCommissionEntity> findActiveCommission(@Param("hierarchy") HierarchyType hierarchyType,
			@Param("serviceId") UUID serviceId, @Param("serviceProviderId") UUID serviceProviderId,
			@Param("slabId") UUID slapId, @Param("agentId") String agentId);

	@Query("SELECT obj  FROM CustomCommissionEntity obj "
			+ "WHERE obj.hierarchyType = :hierarchy and obj.serviceId=:serviceId and obj.agentId=:agentId "
			+ "and obj.serviceProviderId=:serviceProviderId  and obj.slabId = :slabId "
			+ "and obj.fromDate <= :curentDate and obj.toDate >= :curentDate ")
	Optional<CustomCommissionEntity> findActiveCommissionForAgent(@Param("hierarchy") HierarchyType hierarchyType,
			@Param("serviceId") UUID serviceId, @Param("serviceProviderId") UUID serviceProviderId,
			@Param("slabId") UUID slabId, @Param("agentId") String agentId, @Param("curentDate") Calendar calendar);

}
