package com.oasys.commisionmanagement.repository;

import java.util.Calendar;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oasys.commisionmanagement.common.constants.HierarchyType;
import com.oasys.commisionmanagement.entity.CustomCommissionEntity;
import com.oasys.commisionmanagement.entity.DeletedCustomCommissionHistoryEntity;

public interface DeleteCustomCommissionRepository extends JpaRepository<DeletedCustomCommissionHistoryEntity, UUID> {

}
