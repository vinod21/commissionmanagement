package com.oasys.commisionmanagement.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

import com.oasys.commisionmanagement.service.impl.CustomExtractAuthentication;






//import com.oasys.secuirty.service.impl.CustomExtractAuthentication;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "resource-server-rest-api";
	
	@Autowired
	private ResourceServerTokenServices resourceServerTokenServices;

	@Autowired
    private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;
	@Autowired
	private CustomAccessDeniedHandler customAccessDeniedHandler;
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(RESOURCE_ID);
		resources.tokenServices(resourceServerTokenServices);
		resources.authenticationEntryPoint(customAuthenticationEntryPoint);
		resources.accessDeniedHandler(customAccessDeniedHandler);

	}


	@Bean
	@Primary
	public AccessTokenConverter tokenExtractor() {
		return new CustomExtractAuthentication();
	}


	@Override
	public void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().anyRequest().permitAll();

	}
	
}
