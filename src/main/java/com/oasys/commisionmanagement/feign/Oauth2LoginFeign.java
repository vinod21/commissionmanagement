package com.oasys.commisionmanagement.feign;
/*
 * package com.ym.commisionmanagement.feign;
 * 
 * import java.util.Map;
 * 
 * import org.springframework.cloud.openfeign.FeignClient; import
 * org.springframework.http.MediaType; import
 * org.springframework.web.bind.annotation.PostMapping; import
 * org.springframework.web.bind.annotation.RequestHeader;
 * 
 * import com.fasterxml.jackson.databind.JsonNode; import
 * com.ym.commisionmanagement.feign.config.FeignSimpleEncoderConfig;
 * 
 * @FeignClient(name = "Oauth2LoginFeign", url =
 * "${oauth2.login.url}",configuration = FeignSimpleEncoderConfig.class) public
 * interface Oauth2LoginFeign {
 * 
 * @PostMapping(value
 * ="token",consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE) public
 * JsonNode login(@RequestHeader("Authorization") String
 * authorization,@RequestHeader("loginChannel") String loginChannel,Map<String,
 * ?> queryMap); }
 */