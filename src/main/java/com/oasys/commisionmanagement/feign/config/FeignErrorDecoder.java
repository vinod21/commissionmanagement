package com.oasys.commisionmanagement.feign.config;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oasys.commisionmanagement.common.cexception.FeignLayerExcecutionException;
import com.oasys.commisionmanagement.common.constants.ResponseMessageConstant;
import com.oasys.commisionmanagement.common.dto.CBaseDTO;
import com.oasys.commisionmanagement.common.utils.CommonUtil;
import com.oasys.commisionmanagement.feign.dto.FeignAbstractDTO;

import feign.Response;
import feign.codec.ErrorDecoder;

@Component
public class FeignErrorDecoder implements ErrorDecoder {

	@Autowired
	CommonUtil commonUtil;

	@Override
	public Exception decode(String methodKey, Response response) {

		try {
			if (response.body() == null) {
				return new Exception(response.reason() + ":" + response.status());
			}
			String resp = IOUtils.toString(response.body().asInputStream());
			FeignAbstractDTO feignAbstractDTO = commonUtil.jsonToObject(resp, CBaseDTO.class);
			if (feignAbstractDTO.getStatusCode() == ResponseMessageConstant.SUCCESS_RESPONSE.getErrorCode()) {
				return new Exception(resp + ":" + response.status());
			}
			return new FeignLayerExcecutionException(resp, feignAbstractDTO);
		} catch (Exception e) {
			return new Exception(e);
		}

	}

}
