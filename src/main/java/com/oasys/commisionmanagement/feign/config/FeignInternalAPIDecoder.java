package com.oasys.commisionmanagement.feign.config;

import java.io.IOException;
import java.lang.reflect.Type;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.oasys.commisionmanagement.common.cexception.FeignLayerExcecutionException;
import com.oasys.commisionmanagement.common.utils.CommonUtil;
import com.oasys.commisionmanagement.feign.dto.FeignAbstractDTO;

import feign.FeignException;
import feign.codec.DecodeException;
import feign.codec.Decoder;

public class FeignInternalAPIDecoder implements Decoder {

	@Autowired
	private CommonUtil commonUtil;

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object decode(feign.Response response, Type arg1) throws IOException, DecodeException, FeignException {

		if (response.body() == null) {
			return new Exception(response.reason() + ":" + response.status());
		}
		String resp = IOUtils.toString(response.body().asInputStream());
		Object object = commonUtil.jsonToObject(resp, (Class) arg1);
		if (object instanceof FeignAbstractDTO) {
			FeignAbstractDTO feignAbstractDTO = (FeignAbstractDTO) object;
			if (response.status() == HttpStatus.SC_OK && feignAbstractDTO.getStatusCode() == HttpStatus.SC_OK) {
				return feignAbstractDTO;

			}

			if (feignAbstractDTO.getStatusCode() == null || feignAbstractDTO.getMessage() == null) {
				return new Exception(response.reason() + ":" + response.status());
			}
			throw new FeignLayerExcecutionException(resp, feignAbstractDTO);
		}
		if (response.status() == HttpStatus.SC_OK) {
			return object;
		}
		return new Exception(response.reason() + ":" + response.status());
	}

}
