package com.oasys.commisionmanagement.feign.dto;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseContentDTO {
	
	private String applicationName;
	
	private UUID id;
	
	private String ownerName; 
	
	private String username; 
	
	private String agentType;
	
	private UUID roleTypeId;
	
	private ResponseContentDTO parent;
	
	private String roleName;
	
	private String name;
	
	private String serviceProviderName;
	
	private String slot;

}
