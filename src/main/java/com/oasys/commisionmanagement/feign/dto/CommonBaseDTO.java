package com.oasys.commisionmanagement.feign.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommonBaseDTO extends FeignAbstractDTO implements Serializable  {

	private static final long serialVersionUID = 1L;

	Integer statusCode = 500;

	String message;

	ResponseContentDTO responseContent;
	
	Object notification;

	List<?> responseContents;
	
	Integer pageNo;
	
	Integer PageSize;
	
	Integer totalRecords;
	
	Integer totalPages;
	
	Integer numberOfElements;
	
	private String transactionId;
	
	List<Map<String,Map<UUID,String>>> res;
}
