package com.oasys.commisionmanagement.feign.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceAndProviderDto implements Serializable {
	private static final long serialVersionUID = 1L;

	Integer statusCode = 500;

	String message;

	JsonNode responseContent;
	
	
	
}
